// import external modules
import React, { Component } from "react";
import { Link } from "react-router-dom";

import {

   Collapse,
   Navbar,
   Nav,
   UncontrolledDropdown,
   DropdownToggle,
   DropdownMenu,
   DropdownItem,
   Input,
   FormGroup
} from "reactstrap";

import {
   // Moon,
   Menu,
   MoreVertical,
   User,
   LogOut,
} from "react-feather";

import { connect } from "react-redux";
import { setView } from "../../../redux/actions/view/setView";

class ThemeNavbar extends Component {
   handleClick = e => {
      this.props.toggleSidebarMenu("open");
   };
   constructor(props) {
      super(props);

      this.toggle = this.toggle.bind(this);
      this.state = {
         isOpen: false
      };
   }
   toggle() {
      this.setState({
         isOpen: !this.state.isOpen
      });
   }

   render() {
      return (
         <Navbar className="navbar navbar-expand-lg navbar-light bg-faded">
            <div className="container-fluid px-0">
               <div className="navbar-header">
                  <Menu
                     size={14}
                     className="navbar-toggle d-lg-none float-left"
                     onClick={this.handleClick.bind(this)}
                     data-toggle="collapse"
                  />

                  {/* <Moon size={20} color="#333" className="m-2 cursor-pointer"/> */}
                  <MoreVertical
                     className="mt-1 navbar-toggler black no-border float-right"
                     size={50}
                     onClick={this.toggle}
                  />
               </div>

               <div className="navbar-container">

                  <Collapse isOpen={this.state.isOpen} navbar>

                     <Nav className="ml-auto float-right" navbar>

                        <UncontrolledDropdown nav inNavbar className="pr-1">
                           <DropdownToggle nav>

                           <span className="bg-white p-2 align-bottom rounded">
                              
                              <span className="align-bottom mr-2 rounded-circle bg-light p-1 text-bold-500">
                                 <User size={22} className="font-weight-bold" />
                              </span>
                              <span style={{color: "#1CBCD8"}} className="text-bold-400">
                              {(this.props.user && this.props.user.firstname && this.props.user.lastname)?
                                 this.props.user.firstname.charAt(0).toUpperCase()+this.props.user.firstname.slice(1).toLowerCase()+" "
                                 +this.props.user.lastname.charAt(0).toUpperCase()+this.props.user.lastname.slice(1).toLowerCase()
                                 :''
                              }
                              </span>
                           </span>
                              
                           </DropdownToggle>
                           <DropdownMenu right>
                              <DropdownItem divider />

                              <Link to="/pages/user-profile" className="p-0">
                                 <DropdownItem>
                                    <User size={16} className="mr-1" /> Mon Profile
                                 </DropdownItem>
                              </Link>
                              <Link to="/logout" className="p-0">
                                 <DropdownItem>
                                    <LogOut size={16} className="mr-1" /> Se déconnecter
                                 </DropdownItem>
                              </Link>
                           </DropdownMenu>
                        </UncontrolledDropdown>
                     </Nav>
                  </Collapse>
               </div>
            </div>
         </Navbar>
         
      );
   }
}

const mapStateToProps = (state) => ({
   user: state.user.user,
   view: state.view
})

const mapDispatchToProps = (dispatch) => ({
   handleSetView: (view) => dispatch(setView(view)),
})


export default connect(
   mapStateToProps,
   mapDispatchToProps
)(ThemeNavbar);