// import external modules
import React, { Component } from "react";
import { NavLink } from "react-router-dom";

// import internal(own) modules
import { FoldedContentConsumer } from "../../../../utility/context/toggleContentContext";


class SidebarHeader extends Component {
   handleClick = e => {
      this.props.toggleSidebarMenu("close");
   };

   render() {
      return (
         <FoldedContentConsumer>
            {context => (
               <div className="sidebar-header">
                  <div className="logo clearfix">
                     <NavLink to="/" className="logo-text float-left">
                        {/* <div className="logo-img">
                           {templateConfig.sidebar.backgroundColor === "white" ? (
                              this.props.sidebarBgColor === "" || this.props.sidebarBgColor === "white" ? (
                                 <img src={LogoDark} alt="logo" />
                              ) : (
                                 <img src={Logo} alt="logo" />
                              )
                           ) : this.props.sidebarBgColor === "white" ? (
                              <img src={LogoDark} alt="logo" />
                           ) : (
                              <img src={Logo} alt="logo" />
                           )}                           
                        </div> */}
                        <span className="text align-middle text-center">AFI v2</span>
                     </NavLink>
                  </div>
               </div>
            )}
         </FoldedContentConsumer>
      );
   }
}

export default SidebarHeader;
