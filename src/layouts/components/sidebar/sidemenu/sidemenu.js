// import external modules
import React, { Component } from "react";

import {

   Layers, Award, User, Users, Calendar, Book, Sliders, ChevronRight, Settings, CreditCard, Edit3, LogIn,

} from "react-feather";
import { NavLink } from "react-router-dom";

// Styling
import "../../../../assets/scss/components/sidebar/sidemenu/sidemenu.scss";
// import internal(own) modules
import SideMenu from "../sidemenuHelper";
import { connect } from "react-redux";

class SideMenuContent extends Component {
   render() {
      
      return (
            <SideMenu className="sidebar-content" toggleSidebarMenu={this.props.toggleSidebarMenu}>
              <SideMenu.MenuSingleItem badgeColor="danger">
                <NavLink to="/alumni" activeclassname="active">
                    <i className="menu-icon">
                       <Award size={18} />
                    </i>
                    <span className="menu-item-text">Alumni</span>
                 </NavLink>
              </SideMenu.MenuSingleItem>
              <SideMenu.MenuSingleItem badgeColor="danger">
                <NavLink to="/cartes" activeclassname="active">
                    <i className="menu-icon">
                       <CreditCard size={18} />
                    </i>
                    <span className="menu-item-text">Carte étudiants</span>
                 </NavLink>
              </SideMenu.MenuSingleItem>
              <SideMenu.MenuSingleItem badgeColor="danger">
                <NavLink to="/etudiants" activeclassname="active">
                    <i className="menu-icon">
                       <User size={18} />
                    </i>
                    <span className="menu-item-text">Liste des étudiants</span>
                 </NavLink>
              </SideMenu.MenuSingleItem>
              <SideMenu.MenuSingleItem badgeColor="danger">
                <NavLink to="/pre-saisie" activeclassname="active">
                    <i className="menu-icon">
                       <Edit3 size={18} />
                    </i>
                    <span className="menu-item-text">Pré saisie</span>
                 </NavLink>
              </SideMenu.MenuSingleItem>
              <SideMenu.MenuSingleItem badgeColor="danger">
                <NavLink to="/visites" activeclassname="active">
                    <i className="menu-icon">
                       <LogIn size={18} />
                    </i>
                    <span className="menu-item-text">Visites</span>
                 </NavLink>
              </SideMenu.MenuSingleItem>
              <SideMenu.MenuSingleItem badgeColor="danger">
                <NavLink to="/states" activeclassname="active">
                    <i className="menu-icon">
                       <LogIn size={18} />
                    </i>
                    <span className="menu-item-text">Globales</span>
                 </NavLink>
              </SideMenu.MenuSingleItem>
            </SideMenu>

      );
   }
}


const mapStateToProps = (state) => ({
   view: state.view
})

export default connect(
   mapStateToProps,
   null
)(SideMenuContent);