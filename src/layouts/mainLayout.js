// import external modules
import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import classnames from "classnames";

import Spinner from "../components/spinner/spinner";

// import internal(own) modules
import { FoldedContentConsumer, FoldedContentProvider } from "../utility/context/toggleContentContext";
import Sidebar from "./components/sidebar/sidebar";
import Navbar from "./components/navbar/navbar";
import Footer from "./components/footer/footer";
import templateConfig from "../templateConfig";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

// import API functions
import { getCurrentUser } from '../utility/APIutils';
import { setLogin } from '../redux/actions/login/loginActions';
import { setUser } from "../redux/actions/user/userAction";

class MainLayout extends Component {

   constructor(props) {
      super(props);
      this.state = {
         width: window.innerWidth,
         sidebarState: "close",
         sidebarSize: '',
         layout: '',
         isLoading: true
      }
      
      this.loadCurrentUser = this.loadCurrentUser.bind(this);   
   }


   loadCurrentUser = () =>{
      
      getCurrentUser()
      .then(response => {
         console.log("currentUser : ", response)
         this.props.setUserAuthenticated();
         this.props.setUser(response.data)
         this.setState({isLoading: false})
      }).catch(error => {
         console.log("error : ", error)
         this.setState({isLoading: false})
      });
   }

   updateWidth = () => {
      this.setState(prevState => ({
         width: window.innerWidth
      }));
   };

   handleSidebarSize = (sidebarSize) => {
      this.setState({ sidebarSize });
   }

   handleLayout = (layout) => {
      this.setState({ layout });
   }

   componentDidMount() {
      if (window) {
         window.addEventListener("resize", this.updateWidth, false);
      }

      this.loadCurrentUser();
      console.log("setUserAuthenticated : ", this.props)
   }

   componentWillUnmount() {
      if (window) {
         window.removeEventListener("resize", this.updateWidth, false);
      }
   }

   toggleSidebarMenu(sidebarState) {
      this.setState({ sidebarState });
   }

   render() {

      if(this.state.isLoading){
         return <Spinner />
      }

      if(!this.props.isAuthenticated){
         return <Redirect to="/" />
      }

      return (
            <FoldedContentProvider>
               <FoldedContentConsumer>
                  {(context) => (
                  
                     <div
                        className={classnames("wrapper ", {
                           "menu-collapsed": context.foldedContent || this.state.width < 991,
                           "main-layout": !context.foldedContent,
                           [`${templateConfig.sidebar.size}`]: (this.state.sidebarSize === ''),
                           [`${this.state.sidebarSize}`]: (this.state.sidebarSize !== ''),
                        //    "layout-dark": (this.state.layout === 'layout-dark'),
                        //    " layout-dark": (this.state.layout === '' && templateConfig.layoutDark === true)
                           [`${templateConfig.layoutColor}`]: (this.state.layout === ''),
                           [`${this.state.layout}`]: (this.state.layout !== '')
                        })}
                     >

                        <Sidebar
                           toggleSidebarMenu={this.toggleSidebarMenu.bind(this)}
                           sidebarState={this.state.sidebarState}
                           handleSidebarSize={this.handleSidebarSize.bind(this)}
                           handleLayout={this.handleLayout.bind(this)}
                        />
                        <Navbar
                           toggleSidebarMenu={this.toggleSidebarMenu.bind(this)}
                           sidebarState={this.state.sidebarState}
                        />
                        <main>{this.props.children}</main>
                        <Footer />
                     </div>
                  )}
               </FoldedContentConsumer>
            </FoldedContentProvider>
      );
   }
}

const mapStateToProps = (state) => ({
   isAuthenticated: state.login.isAuthenticated
})

const mapDispatchToProps = (dispatch) => ({
   setUserAuthenticated: bindActionCreators(setLogin, dispatch),
   setUser: bindActionCreators(setUser, dispatch),
})

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(MainLayout);
