// import external modules
import React, { Fragment, Component } from "react";
import { 
    NAME_MIN_LENGTH, NAME_MAX_LENGTH, 
    USERNAME_MIN_LENGTH, USERNAME_MAX_LENGTH,
    PASSWORD_MIN_LENGTH, PASSWORD_MAX_LENGTH
 } from '../../constants';
 import { register, checkLoginAvailability } from '../../utility/APIutils';
import {
   Row,
   Col,
   Button,
   Form,
   Input,
   FormGroup,
   Card,
   CardBody,
   CardFooter,
   CardTitle,
   Label,
   UncontrolledTooltip,
   UncontrolledAlert
} from "reactstrap";
import Select from "react-select";
import { BounceLoader } from 'react-spinners';
import {
    AlertCircle,
 } from "react-feather";
 import { connect } from 'react-redux';
 import {toastr} from 'react-redux-toastr';

class NewAccount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstname: {
                value: '',
                hasError: false,
                errorMsg: ""
            },
            lastname: {
                value: '',
                hasError: false,
                errorMsg: ""
            },
            login: {
                value: '',
                hasError: false,
                errorMsg: ""
            },
            password: {
                value: '',
                hasError: false,
                errorMsg: ""
            },
            rolesSelect:   [{ value: 'ROLE_ADMIN', label: 'Administrateur' },
                     { value: 'ROLE_USER', label: 'Utilisateur' }],

            permissionsSelect:   [{ value: 'read', label: 'Lecture' },
            { value: 'write', label: 'Ecriture' }],
            
            roles: [],
            permissions: [],
            isLoading: false,

            hasErrorAPI: {
                value: false,
                message: ""
             }
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.checkInput = this.checkInput.bind(this);
        this.handleRolesChange = this.handleRolesChange.bind(this);
        this.handlePermChange = this.handlePermChange.bind(this);
        // this.validateUsernameAvailability = this.validateUsernameAvailability.bind(this);
        // this.validateEmailAvailability = this.validateEmailAvailability.bind(this);
        this.isFormInvalid = this.isFormInvalid.bind(this);

    }

    componentDidMount(){
        console.log("auth count : ", this.props)
    }


    handleInputChange(event) {
        const target = event.target;
        const inputName = target.name;        
        const inputValue = target.value;
  
        this.setState({
           [inputName] : {
              value: inputValue,
           }
        }, () => { console.log(inputName, inputValue) });
    }

    checkInput(event, validationFun){
        
        const target = event.target;
        const inputName = target.name;        
        const inputValue = target.value;

        this.setState({
            [inputName] : {
               value: inputValue,
               ...validationFun(inputValue)
            }
        }, () => { console.log(inputName, this.state.login) });
    }

    handleRolesChange = (newValue) => {

        let tempRoles = [];

        newValue.map((item) =>{
            tempRoles.push(item.value)
        })

        this.setState({
            roles : tempRoles
        }, () => { console.log("my roles", this.state.roles) });
    };


    handlePermChange = (newValue) => {

        let tempPerms = [];

        newValue.map((item) =>{
            tempPerms.push(item.value)
        })

        this.setState({
            permissions : tempPerms
            
        }, () => { console.log("my roles", this.state.permissions) });
    };


    isFormInvalid() {
        return !(this.state.firstname.hasError === false &&
            this.state.lastname.hasError === false &&
            this.state.login.hasError === false &&
            this.state.password.hasError === false
        );
    }

    handleSubmit(event) {
        event.preventDefault();
    
        if(this.isFormInvalid()){
           return;
        }
  
        this.setState({
            isLoading: true
        })

        const request = {
            firstname: this.state.firstname.value,
            lastname: this.state.lastname.value,
            username: this.state.login.value,
            password: this.state.password.value,
            roles: this.state.roles,
            permissions: this.state.permissions
        };
        console.log("request ", request)
        register(request)
        .then(response => {
           

            console.log("response ", response);
            this.setState({
                isLoading: false,
                firstname: {
                    value: '',
                    hasError: false,
                    errorMsg: ""
                },
                lastname: {
                    value: '',
                    hasError: false,
                    errorMsg: ""
                },
                login: {
                    value: '',
                    hasError: false,
                    errorMsg: ""
                },
                password: {
                    value: '',
                    hasError: false,
                    errorMsg: ""
                },
                roles: [],
                permissions: []
            })

            toastr.success('', "Le compte utilisateur a bien étè ajouté", { position: 'bottom-center', transitionIn: 'bounceIn', transitionOut: 'bounceOut' })

        }).catch(error => {
            console.log("error ", error.response);
            this.setState({
                isLoading: false
            })

            this.setState({
                hasErrorAPI: {value: true, message: error.response.data.message}
            });
        });
    }
    

   render() {
      return (
        <Fragment>
        <Row className="d-flex justify-content-center">
           <Col xs="12" >

            {
                (this.state.hasErrorAPI.value)?
                <UncontrolledAlert color="danger">
                    <span className="white" style={{color: "#fff"}} color="white">
                       {this.state.hasErrorAPI.message}
                    </span>
                </UncontrolledAlert>:''
            }

              <Card className="">
              <Form onSubmit={this.handleSubmit} className="form-bordered form-horizontal">
                <CardBody>
                    <CardTitle>Créer un compte utilisateur</CardTitle>
                    <hr />
                    <div className="">						
                        
                        <div className="form-body">                            
                            <Row>
                                <Col md="6">
                                    <FormGroup row>
                                        <Label className="pl-0" for="userinput1" sm={4}>Prénom</Label>
                                        <Col  sm={8}>
                                            <Input type="text" 
                                                id="userinput1" 
                                                disabled={this.state.isLoading}
                                                value={this.state.firstname.value}
                                                className="border-primary"  
                                                name="firstname"
                                                onBlur={(event) =>  this.checkInput(event, this.validateName)}
                                                onChange={(event) => this.handleInputChange(event)}
                                                required
                                            />
                                            {(this.state.firstname.hasError)? (
                                                <div style={{marginTop: "19px"}} className="form-control-position">
                                                <AlertCircle id="firstname"  className="danger"/>
                                                <UncontrolledTooltip
                                                    placement="right"
                                                    target="firstname"
                                                >
                                                    {this.state.firstname.errorMsg}
                                                </UncontrolledTooltip>
                                                </div>):''
                                            }
                                        </Col>
                                    </FormGroup>
                                </Col>
                                <Col md="6">
                                    <FormGroup row>
                                        <Label className="pl-0"  for="userinput2" sm={4}>Nom</Label>
                                        <Col sm={8}>
                                            <Input type="text" 
                                            id="userinput2" 
                                            className="border-primary"  
                                            value={this.state.lastname.value}
                                            disabled={this.state.isLoading}
                                            name="lastname"
                                            onBlur={(event) =>  this.checkInput(event, this.validateName)}
                                            onChange={(event) => this.handleInputChange(event)}
                                            required
                                            />
                                            {(this.state.lastname.hasError)? (
                                                <div style={{marginTop: "19px"}} className="form-control-position">
                                                <AlertCircle id="lastname"  className="danger"/>
                                                <UncontrolledTooltip
                                                    placement="right"
                                                    target="lastname"
                                                >
                                                    {this.state.lastname.errorMsg}
                                                </UncontrolledTooltip>
                                                </div>):''
                                            }
                                        </Col>
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                            <Col md="6">
                                <FormGroup row className="last">
                                    <Label className="pl-0"  for="userinput3" sm={4}>Login</Label>
                                    <Col sm={8}>
                                        <Input type="text" 
                                        id="userinput3" 
                                        className="border-primary"  
                                        value={this.state.login.value}
                                        disabled={this.state.isLoading}
                                        name="login"
                                        onBlur={(event) =>  this.checkInput(event, this.validateLogin)}
                                        onChange={(event) => this.handleInputChange(event)}
                                        required
                                        />
                                        {(this.state.login.hasError)? (
                                            <div style={{marginTop: "19px"}} className="form-control-position">
                                            <AlertCircle id="login"  className="danger"/>
                                            <UncontrolledTooltip
                                                placement="right"
                                                target="login"
                                            >
                                                {this.state.login.errorMsg}
                                            </UncontrolledTooltip>
                                            </div>):''
                                        }
                                    </Col>
                                </FormGroup>
                            </Col>
                            <Col md="6">
                                <FormGroup row className="last">
                                    <Label className="pl-0"  for="userinput4" sm={4}>Mot de passe</Label>
                                    <Col sm={8}>
                                        <Input type="text" 
                                        id="userinput4" 
                                        className="border-primary"  
                                        value={this.state.password.value}
                                        disabled={this.state.isLoading}
                                        name="password"
                                        onBlur={(event) =>  this.checkInput(event, this.validatePassword)}
                                        onChange={(event) => this.handleInputChange(event)}
                                        required
                                        />
                                        {(this.state.password.hasError)? (
                                            <div style={{marginTop: "19px"}} className="form-control-position">
                                            <AlertCircle id="password"  className="danger"/>
                                            <UncontrolledTooltip
                                                placement="right"
                                                target="password"
                                            >
                                                {this.state.password.errorMsg}
                                            </UncontrolledTooltip>
                                            </div>):''
                                        }
                                    </Col>
                                </FormGroup>
                            </Col>
                            <Col md="6">
                                <FormGroup row className="last">
                                    <Label className="pl-0"  for="userinput6" sm={4}>Roles</Label>
                                    <Col sm={8}>
                                        <Select
                                            isMulti
                                            name="roles"
                                            disabled={this.state.isLoading}
                                            options={this.state.rolesSelect}
                                            // value={this.state.roles}
                                            onChange={this.handleRolesChange}
                                            className="basic-multi-select border-primary"
                                            classNamePrefix="Sélectionner"
                                            required
                                            isClearable={true}
                                        />
                                    </Col>
                                </FormGroup>
                            </Col>
                            <Col md="6">
                                <FormGroup row className="last">
                                    <Label className="pl-0"  for="userinput5" sm={4}>Permissions</Label>
                                    <Col sm={8}>
                                    <Select
                                            isMulti
                                            name="roles"
                                            disabled={this.state.isLoading}
                                            onChange={this.handlePermChange}
                                            options={this.state.permissionsSelect}
                                            className="basic-multi-select border-primary"
                                            classNamePrefix="Sélectionner"
                                            required
                                            isClearable={true}
                                        />
                                    </Col>
                                </FormGroup>
                            </Col>
                            </Row>
                        </div>
                    </div>
                </CardBody>
                <CardFooter className="d-flex bg-light justify-content-end">
                   {(this.state.isLoading)?
                        (<BounceLoader  					
                            className="clip-loader right"
                            sizeUnit={"px"}
                            size={25}
                            color={'#7e7e86'}
                            loading={true} 
                        />):''

                    }
                    <FormGroup className="ml-3 mb-0 right">
                        <Button  style={{boxShadow:"0 2px 2px rgba(0,0,60,.08)",fontFamily: 'Montserrat', background:"rgb(30, 131, 172)", display: 'flex', alignItem:"center", borderRadius: "4px", justifyContent: 'center',}} type="submit" className="mb-0 py-1 px-3 btn-raised">
                            VALIDER
                        </Button>
                    </FormGroup>
                </CardFooter>
                </Form>
              </Card>
           </Col>
        </Row>
     </Fragment>
      );
   }



   validateName = (name) => {

        if(name.length < NAME_MIN_LENGTH) {
            return {
                hasError: true,
                errorMsg: `Le nom est trop court (Minimum ${NAME_MIN_LENGTH} caractères nécessaires.)`
            }
        } else if (name.length > NAME_MAX_LENGTH) {
            return {
                hasError: true,
                errorMsg: `Le nom est trop long (Maximum ${NAME_MAX_LENGTH} caractères nécessaires.)`
            }
        } else {
            return {
                hasError: false,
                errorMsg: null,
            };            
        }
    }

    validateLogin = (username) => {
        if(username.length < USERNAME_MIN_LENGTH) {
            return {
                hasError: true,
                errorMsg: `Le nom d'utilisateur est trop court (Minimum ${USERNAME_MIN_LENGTH} caractères nécessaires.)`
            }
        } else if (username.length > USERNAME_MAX_LENGTH) {
            return {
                hasError: true,
                errorMsg: `Le nom d'utilisateur est trop long (Maximum ${USERNAME_MAX_LENGTH} caractères nécessaires.)`
            }
        } else {

            checkLoginAvailability(username)
            
            .then(response => {
    
                console.log("response ", response)
    
                return {
                    hasError: false,
                    errorMsg: null,
                };  
                
            }).catch(error => {
       
                console.log("error ", error.response)

                this.setState({
                    login:{
                        value: username,
                        hasError: true,
                        errorMsg: error.response.data.message
                    }
                }) 
                
                return;
            });  
        }

        return {
            hasError: false,
            errorMsg: null,
        };
    }

    validatePassword = (password) => {
        if(password.length < PASSWORD_MIN_LENGTH) {
            return {
                hasError: true,
                errorMsg: `Le mot de passe est trop court (Minimum ${PASSWORD_MIN_LENGTH} caractères nécessaires.)`
            }
        } else if (password.length > PASSWORD_MAX_LENGTH) {
            return {
                hasError: true,
                errorMsg: `Le mot de passe est trop long (Maximum ${PASSWORD_MAX_LENGTH} caractères nécessaires.)`
            }
        } else {
            return {
                hasError: false,
                errorMsg: null,
            };            
        }
    }

}

const mapStateToProps = (state) => ({
    isAuthenticated: state.login.isAuthenticated,
    user: state.user.user
})
 
export default connect(
    mapStateToProps,
    null
)(NewAccount);