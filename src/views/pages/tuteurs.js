// import external modules
import React, { Fragment, Component } from "react";
import { Row, Card, Form, FormGroup, Label, Input, CardBody, Col, Button, CardHeader, Table } from "reactstrap";
import { CloudOff } from "react-feather";

class Tuteurs extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }

    }



   render() {
      return (
        <Fragment>
          <Row className="">
            <Col className="col-sm-12 col-xs-12 col-md-12 col-lg-12">
              <Card className="">
                <CardHeader className="bg-light">
                  <Form>
                    <Row>
                      <Col className="col-sm-3 col-xs-3 col-md-3 col-lg-3">
                        <FormGroup className="mb-0">
                          <Label for="timesheetinput1">Année académique</Label>
                            <div className="position-relative mb-0">
                              <Input type="select" id="projectinput5" name="interested" >
                                  <option value="2019-2020" defaultValue="" disabled="">2019-2020</option>
                                  <option value="2018-2019">2017-2018</option>
                                  <option value="2016-2017">2016-2017</option>
                              </Input>
                            </div>
                        </FormGroup>
                      </Col>
                      <Col className="col-sm-3 col-xs-3 col-md-3 col-lg-3">
                        <FormGroup className="mb-0">
                          <Label for="timesheetinput1">Classe</Label>
                          <div className="position-relative mb-0">
                              <Input type="text" name="employeename" />
                          </div>
                        </FormGroup>
                      </Col>
                      <Col className="col-sm-6 col-xs-6 col-md-6 col-lg-6 d-flex justify-content-start">
                        <FormGroup className="align-self-end mb-0">
                          <div className="position-relative mb-0 has-icon-left">
                            <Button style={{boxShadow:"0 2px 2px rgba(0,0,60,.08)",fontFamily: 'Montserrat', background:"rgb(30, 131, 172)", display: 'flex', alignItem:"center", borderRadius: "4px", justifyContent: 'center',}} className="mb-0 py-1 px-3 btn-raised">
                                AFFICHER
                            </Button>
                          </div>
                        </FormGroup>
                      </Col>
                    </Row>
                  </Form>
                </CardHeader>
                <CardBody className="">
                  <Row className="">
                    <Col className="col-sm-12 col-xs-12 col-md-12 col-lg-12">
                      <Table striped>
                        <thead>
                          <tr>
                            <th>Matricule </th>
                            <th>Prénom & Nom</th>
                            <th>Téléphone</th>
                            <th>Adresse de l'étudiant</th>
                            <th>Tutueur</th>
                            <th>Fonction du tutueur</th>
                            <th>Entreprise</th>
                            <th>Téléphone du tuteur</th>
                            <th>Adresse du tuteur</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </Table>
                      <div style={{width: "100%"}} className="d-flex justify-content-center">
                        <div>
                          <CloudOff color="#cfcfcf" size={100} />
                          <br />
                          	
                          <p className="text-muted mt-1">Aucune donnée</p>
                        </div>
                      </div>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Fragment>
      );
   }

}
 
export default Tuteurs;