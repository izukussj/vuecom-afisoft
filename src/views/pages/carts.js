import React, { Fragment, Component } from "react";
import { Row, Card, Form, FormGroup, Label, Input, CardBody, Col, Button, CardHeader, Table, CardTitle, CardText } from "reactstrap";
import { CloudOff } from "react-feather";
 

import cardImgEle01 from "../../../src/assets/img/carte.png";
class Carts extends Component {

    constructor(props) {
        super(props);
        this.state = {

        }

    }



   render() {
      return (
        <Fragment>
          <Row className="">
            <Col className="col-sm-12 col-xs-12 col-md-9 col-lg-9">
              <Card className="">
                <CardHeader className="bg-light">
                    <Form>
                        <Row>
                        <Col className="col-sm-3 col-xs-3 col-md-3 col-lg-3">
                            <FormGroup className="mb-0">
                            <Label for="timesheetinput1">Sélectionner une classe</Label>
                                <div className="position-relative mb-0">
                                <Input type="select" id="projectinput5" name="interested" >
                                    <option value="2019-2020" defaultValue="" disabled="">2019-2020</option>
                                    <option value="2018-2019">2017-2018</option>
                                    <option value="2016-2017">2016-2017</option>
                                </Input>
                                </div>
                            </FormGroup>
                        </Col>
                        <Col className="col-sm-3 col-xs-3 col-md-3 col-lg-3 d-flex justify-content-start">
                            <FormGroup className="align-self-end mb-0">
                            <div className="position-relative mb-0 has-icon-left">
                                <Button style={{boxShadow:"0 2px 2px rgba(0,0,60,.08)",fontFamily: 'Montserrat', background:"rgb(30, 131, 172)", display: 'flex', alignItem:"center", borderRadius: "4px", justifyContent: 'center',}} className="mb-0 py-1 px-3 btn-raised">
                                    AFFICHER
                                </Button>
                            </div>
                            </FormGroup>
                        </Col>
                        <Col className="col-sm-6 col-xs-6 col-md-6 col-lg-6 d-flex justify-content-end">
                            <FormGroup className="align-self-end mb-0 mr-3">
                            <div className="position-relative mb-0 has-icon-left">
                                <Button style={{boxShadow:"0 2px 2px rgba(0,0,60,.08)",fontSize:"12px",fontFamily: 'Montserrat', background:"rgb(30, 131, 172)", display: 'flex', alignItem:"center", borderRadius: "4px", justifyContent: 'center',}} className="mb-0 py-1 px-2 btn-raised">
                                    IMPRIMER UNE NOUVELLE CARTE
                                </Button>
                            </div>
                            </FormGroup>
                            <FormGroup className="align-self-end mb-0">
                            <div className="position-relative mb-0 has-icon-left">
                                <Button style={{boxShadow:"0 2px 2px rgba(0,0,60,.08)",fontSize:"12px", fontFamily: 'Montserrat', background:"rgb(30, 131, 172)", display: 'flex', alignItem:"center", borderRadius: "4px", justifyContent: 'center',}} className="mb-0 py-1 px-2 btn-raised">
                                    IMPRIMER LE QR CODE
                                </Button>
                            </div>
                            </FormGroup>
                        </Col>
                        </Row>
                    </Form>
                </CardHeader>
                <CardBody className="">
                    <Col sm="12" md="8">
                        <Card className="card box-shadow-0 text-center p-0">
                            <CardBody className="bg-light">
                                <div style={{position: "relative"}} className="row d-flex">
                                    <div style={{position: "relative", left:"0", top:"0"}} className="m-0">
                                        <img style={{boxShadow:"0 2px 2px rgba(0,0,60,.08)"}} src={cardImgEle01} width="100%" alt="Card cap 01" className="" />
                                    </div>
                                    <div style={{zIndex:"9999", height:"45%",width:"20%",border:"1px solid red",position: "absolute", left:"2%", bottom:"22%"}} className="m-0">
                                    </div>
                                    <div style={{zIndex:"9999", height:"45%",width:"20%",border:"1px solid green",position: "absolute", left:"25%", bottom:"5%"}} className="m-0">
                                    </div>
                                    <div style={{zIndex:"9999", height:"45%",width:"20%",border:"1px solid blue",position: "absolute", left:"70%", bottom:"5%"}} className="m-0">
                                    </div>

                                    <div style={{zIndex:"9999", height:"13%",width:"20%",border:"1px solid blue",position: "absolute", left:"2%", bottom:"5%"}} className="m-0">
                                    </div>

                                </div>
                            </CardBody> 
                        </Card>
                    </Col>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Fragment>
      );
   }

}
 
export default Carts;