// import external modules
import React, { Component } from "react";
import { connect } from 'react-redux'
import { NavLink, Redirect } from "react-router-dom";
import { login } from '../../utility/APIutils';
import { ACCESS_TOKEN } from '../../constants';
import { setLogin } from '../../redux/actions/login/loginActions';
import { bindActionCreators } from 'redux'

import {
   Row,
   Col,  
   Input,
   Form,
   FormGroup,
   Button,
   Card,   
   CardBody,
   CardFooter
} from "reactstrap";
import {
   Eye,
   EyeOff
} from "react-feather";
import { BounceLoader } from "react-spinners";


class Login extends Component {

  constructor(props){
      super(props);
      this.state = {
         login:"",
         password:"",
         loginError: {
            message: "",
            status: false
         },
         hidden: true,
         authSuccess: false,
         isLoading:false,
      }
      
      this.handleSubmit = this.handleSubmit.bind(this);
  }



  handleSubmit (event) {
   event.preventDefault();   

   
      const loginRequest = {
         grant_type: "password",
         username: this.state.login,
         password: this.state.password
      } 

      this.setState({isLoading: true});

      login(loginRequest)

      .then( response => {
         
         localStorage.setItem(ACCESS_TOKEN, response.data.access_token);
         localStorage.setItem("refresh_token", response.data.refresh_token);
         localStorage.setItem("expires_in", response.data.expires_in);
         localStorage.setItem("username", loginRequest.username);
         this.setState({
            loginError:{
               message: "",
               status: false
            },
            login:"",
            password:"",
            authSuccess: true
         })
         this.props.handleLogin();
         this.setState({isLoading: false});
      }).catch(error => {

         if(error.response.status === 401 || error.response.status === 400){

            this.setState({
               loginError:{
                  message: "Identifiant ou mot de passe incorrect ! ",
                  status: true
               }
            })
         }
         this.setState({isLoading: false});
      });
   }

   

   render() {

      if(this.props.isAuthenticated){
         return <Redirect to="/account/new/" />
      }

      return (
         <div className="container">
            <Row className="full-height-vh">
               <Col xs="12" className="d-flex align-items-center justify-content-center">
                  <Card className="gradient-indigo-purple text-center width-400">
                     <CardBody>
                        <h2 className="white py-4">Connexion</h2>
                        {(this.state.loginError.status)?
                        <p className="white">Identifiant ou mot de passe incorrect !</p>:""
                        }
                        
                        <Form onSubmit={this.handleSubmit} className="pt-2">
                           <FormGroup>
                              <Col md="12">
                                 <Input
                                    type="text"
                                    className="form-control py-3"
                                    name="login"
                                    value={this.state.login}
                                    onChange={event => this.setState({login: event.target.value, loginError: {message: "",status: false}})}
                                    id="login"
                                    placeholder="Login"
                                    required
                                 />
                              </Col>
                           </FormGroup>

                           <FormGroup>
                              <Col md="12">
                                 <Input
                                    type={this.state.hidden ? "password":"text"}
                                    className="form-control py-3"
                                    name="password"
                                    onChange={event => this.setState({password: event.target.value, loginError: {message: "",status: false}})}
                                    value={this.state.password}
                                    id="password"
                                    placeholder="Mot de passe"
                                    required
                                 />

                                 {(this.state.hidden)? (
                                    <div onClick={(event) =>{this.setState({hidden:!this.state.hidden,})}} className="cursor-pointer form-control-position mt-1 mr-2">
                                    <Eye size={22} id="eye"/>
                                    </div>):''
                                 }

                                 {(!this.state.hidden)? (
                                    <div onClick={(event) =>{this.setState({hidden:!this.state.hidden})}} className="cursor-pointer form-control-position mt-1 mr-2">
                                    <EyeOff size={22} id="eye"/>
                                    </div>):''
                                 }
                                 
                              </Col>
                           </FormGroup>

                           <FormGroup>
                              <Col md="12">
                                 <Button disabled={this.state.isLoading} type="submit" color="primary" block className="btn-raised" style={{boxShadow:"0 2px 2px rgba(0,0,60,.08)",fontFamily: 'Montserrat', background:"rgb(30, 131, 172)", height:"52px", display: 'flex', alignItem:"center", borderRadius: "4px", justifyContent: 'center',}}>
                                    
                                    
                                    {(this.state.isLoading)?
                                          (<BounceLoader  					
                                                className="clip-loader center"
                                                sizeUnit={"px"}
                                                size={25}
                                                color={'#fff'}
                                                loading={true} 
                                          />):'Se connecter'

                                       }
                                 </Button>
                              </Col>
                           </FormGroup>
                        </Form>
                     </CardBody>
                     <CardFooter>
                        <div className="float-left">
                           <NavLink to="/forgot-password" className="text-white">
                              Mot de passe oublié ?
                           </NavLink>
                        </div>
                     </CardFooter>
                  </Card>
               </Col>
            </Row>
         </div>
      );
   }
}

const mapStateToProps = (state) => ({
   isAuthenticated: state.login.isAuthenticated
})

const mapDispatchToProps = (dispatch) => ({

   handleLogin: bindActionCreators(setLogin, dispatch),

})

export default connect(
   mapStateToProps,
   mapDispatchToProps
)(Login);
