import React, { Component} from "react";
import  { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import {logOut as LogoutAction} from "../../redux/actions/login/loginActions";

class Logout extends Component {

    constructor(props) {
      super(props);
      localStorage.removeItem("accessToken");
      this.props.setLogout();
    }

    render() {

      return (
        <Redirect to='/'/>
      );
    }
}

  
const mapStateToProps = state => ({
    currentUser: state.currentUser,
 })
 
const mapDispatchToProps = dispatch => ({
    setLogout: () => dispatch(LogoutAction())
 })
  
 export default connect(
    mapStateToProps,
    mapDispatchToProps
 )(Logout)