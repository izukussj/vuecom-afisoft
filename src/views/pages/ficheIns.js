// import external modules
import React, { Fragment, Component } from "react";
import { Row, Card, Form, FormGroup, Label, Input, CardBody, Col, Button, CardHeader, Table, CustomInput } from "reactstrap";
import { User, CloudOff, Award, Users, Image } from "react-feather";
import Dropzone from "react-dropzone";

class FicheIns extends Component {

    constructor(props) {
        super(props);
        this.state = {
            files: []
        }

    }
  
     onDrop = files => {
        this.setState({
           files
        });
     };
  


   render() {
      return (
        <Fragment>

            <Row>
                <Col md="8">
                    <Card className="">
                        <CardBody className="">
                            <h4 className="form-section mb-3 bg-light"><User size={20} color="#212529" /> Identification de l'étudiant</h4>
                            <Row>
                                <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                    <FormGroup>
                                       <Label for="projectinput2">Année académique</Label>
                                        <Input type="select" id="projectinput5" name="interested" >
                                            <option value="2019-2020" defaultValue="" disabled="">2019-2020</option>
                                            <option value="2018-2019">2017-2018</option>
                                            <option value="2016-2017">2016-2017</option>
                                        </Input>
                                    </FormGroup>
                                </Col>
                                <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                    <FormGroup>
                                        <Label for="projectinput2">Nom(s)</Label>
                                        <Input type="text" />
                                    </FormGroup>
                                </Col>
                                <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                    <FormGroup>
                                        <Label for="projectinput1">Prénom(s)</Label>
                                        <Input type="text" />
                                    </FormGroup>
                                </Col>
                                <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                    <FormGroup>
                                        <Label for="projectinput2">Date</Label>
                                        <Input type="date" />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                    <FormGroup>
                                        <Label for="projectinput3">Lieu de naissance</Label>
                                        <Input type="text" />
                                    </FormGroup>
                                </Col>
                                <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                    <FormGroup>
                                        <Label for="projectinput4">Pays de Naissance</Label>
                                        <Input type="text" />
                                    </FormGroup>
                                </Col>
                                <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                    <FormGroup>
                                        <Label for="projectinput3">Nationalité</Label>
                                        <Input type="text" />
                                    </FormGroup>
                                </Col>
                                <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                    <FormGroup>
                                        <Label for="projectinput4">Adresse</Label>
                                        <Input type="text" />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                    <FormGroup>
                                        <Label for="projectinput3">Téléphone</Label>
                                        <Input type="text" />
                                    </FormGroup>
                                </Col>
                                <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                    <FormGroup>
                                        <Label for="projectinput4">Email</Label>
                                        <Input type="text" />
                                    </FormGroup>
                                </Col>
                                <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                    <FormGroup>
                                        <Label for="projectinput3">Classe</Label>
                                        <Input type="text" />
                                    </FormGroup>
                                </Col>
                                <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                    <FormGroup>
                                        <Label for="projectinput4">Option</Label>
                                        <Input type="text" />
                                    </FormGroup>
                                </Col>
                            </Row>

                            <h4 className="form-section mb-3 bg-light"><Award size={20} color="#212529" /> Identification du tuteur ou de l'entreprise</h4>
                            <Row>
                                <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                    <FormGroup>
                                        <Label for="projectinput3">Nom du tutuer ou de l'entreprise</Label>
                                        <Input type="text" />
                                    </FormGroup>
                                </Col>
                                <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                    <FormGroup>
                                        <Label for="projectinput4">Employeur</Label>
                                        <Input type="text" />
                                    </FormGroup>
                                </Col>
                                <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                    <FormGroup>
                                        <Label for="projectinput3">Fonction</Label>
                                        <Input type="text" />
                                    </FormGroup>
                                </Col>
                                <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                    <FormGroup>
                                        <Label for="projectinput4">Adresse tuteur</Label>
                                        <Input type="text"/>
                                    </FormGroup>
                                </Col>
                                <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                    <FormGroup>
                                        <Label for="projectinput4">Téléphone du tuteur</Label>
                                        <Input type="text"/>
                                    </FormGroup>
                                </Col>
                            </Row>
                        </CardBody> 
                    </Card>
                </Col>

                <Col md="4">
                    <Card className="">
                        <CardBody className="">                   
                            <FormGroup tag="fieldset">
                                <Row>
                                    <Col className="col-md-12 col-lg-12 col-sm-12 col-12">
                                        <div>
                                            <div className="dropzone">
                                                <Dropzone onDrop={this.onDrop.bind(this)}>
                                                    <div style={{height:"100%"}} className="d-flex cursor-pointer justify-content-center">
                                                        <div className="d-flex align-items-center">
                                                           <Image color="#cfcfcf" size={75} />
                                                        </div>
                                                    </div>
                                                </Dropzone>
                                            </div>
                                        </div>
                                    </Col>
                                </Row>
                                <h6 className="mt-3">Situation de famille</h6>
                                <Row>
                                    <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                        <FormGroup check className="px-0">
                                            <CustomInput type="radio" id="exampleCustomRadio" name="customRadio" label="Célibataire" defaultChecked />
                                        </FormGroup>
                                    </Col>
                                    <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                        <FormGroup check className="px-0">
                                            <CustomInput type="radio" id="exampleCustomRadio2" name="customRadio" label="Marié" />
                                        </FormGroup>
                                    </Col>
                                    <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                        <FormGroup check className="px-0">
                                            <CustomInput type="radio" id="exampleCustomRadio3" name="customRadio" label="Divorcé" />
                                        </FormGroup>
                                    </Col>
                                    <Col className="col-md-3 col-lg-3 col-sm-6 col-12">
                                        <FormGroup check className="px-0">
                                            <CustomInput type="radio" id="exampleCustomRadio4" name="customRadio" label="Veuf" />
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </FormGroup>
                        </CardBody> 
                    </Card>

                    <Card className="">
                        <CardBody className="">                   
                            <h4 className="form-section mb-3 bg-light"><User size={20} color="#212529" /> scolarité</h4>
                            <Row>
                                <Col className="col-md-6 col-lg-6 col-sm-6 col-12">
                                    <FormGroup>
                                        <Label for="projectinput2">Niveau d'études</Label>
                                        <Input type="text" />
                                    </FormGroup>
                                </Col>
                                <Col className="col-md-6 col-lg-6 col-sm-6 col-12">
                                    <FormGroup>
                                        <Label for="projectinput1">Ancienne école</Label>
                                        <Input type="text"  />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <h6></h6>
                            <Row>
                                <Col className="col-md-12 col-lg-12 col-sm-12 col-12">
                                    <FormGroup tag="fieldset">
                                        <h6>L'étudiant bénéficie t-il d'une prise en charge ?</h6>
                                        <FormGroup check className="px-0">
                                            <CustomInput type="radio" id="exampleCustomRadioYes" name="customRadioCharge" label="Oui" defaultChecked />
                                        </FormGroup>
                                        <FormGroup check className="px-0">
                                            <CustomInput type="radio" id="exampleCustomRadioNon" name="customRadioCharge" label="Non" />
                                        </FormGroup>
                                    </FormGroup>
                                </Col>
                            </Row>
                        </CardBody> 
                    </Card>
                </Col>
            </Row>

        </Fragment>
      );
   }

}
 
export default FicheIns;