// import external modules
import React, { Component } from "react";
import { changerPassword } from '../../utility/APIutils';
import { Row, Col, Input, Form, FormGroup, Button, Card, CardBody } from "reactstrap";
import { BounceLoader } from "react-spinners";


class ResetPassword extends Component  {

   constructor(props) {

      super(props);
      this.state = {
        password: "",
        passwordConfirmation: "",
        token: this.props.match.params.token,
        isLoading:false,
      }

      this.handleSubmit = this.handleSubmit.bind(this);
      console.log("token ", this.state.token)
   }


   handleSubmit(event) {
      event.preventDefault();   

      const passwordRequest  = {
            password: this.state.password,
            token: this.state.token
      };
      
      console.log("passwordRequest : ", passwordRequest);
      this.setState({isLoading: true});


      changerPassword(passwordRequest)
      .then(response => {

            this.props.history.push("/");
            this.setState({isLoading: false});         
            console.log("réponse : ", response);
      }).catch(error => {
            console.log("error : ", error);
            this.setState({isLoading: false});

      });
   }


   render(){
      return (
         <div className="container">
            <Row className="full-height-vh">
               <Col xs="12" className="d-flex align-items-center justify-content-center ">
                  <Card className="gradient-indigo-purple text-center width-400">                  
                     <CardBody>
                        <div className="text-center">
                           <h6 className="text-uppercase text-bold-500 white py-2">Nouveau mot de passe</h6>
                        </div>
                        <Form className="pt-2" onSubmit={this.handleSubmit}>

                        <FormGroup>
                                 <Col md="12">
                                    <Input
                                       type="password"
                                       className="form-control py-3"
                                       name="password"
                                       value={this.state.password}
                                       disabled={this.state.isLoading}
                                       onChange={event => this.setState({password: event.target.value}, () => {console.log("password ", this.state.password)})}
                                       id="inputPass"
                                       placeholder="Mot de passe"
                                       required
                                    />

                                 </Col>
                              </FormGroup>

                              <FormGroup>
                                 <Col md="12">
                                    <Input
                                       type="password"
                                       className="form-control py-3"
                                       name="passwordConfirmation"
                                       disabled={this.state.isLoading}
                                       value={this.state.passwordConfirmation}
                                       onChange={event => this.setState({passwordConfirmation: event.target.value}, () => {console.log("passwordConfirmation ", this.state.passwordConfirmation)})}
                                       id="inputConf"
                                       placeholder="Retapez mot de passe"
                                       required
                                    />
                                 </Col>
                              </FormGroup>
                           <FormGroup className="pt-2">
                              <Col md="12">
                                 <div className="text-center">
                                    <Button disabled={this.state.isLoading} style={{boxShadow:"0 2px 2px rgba(0,0,60,.08)",fontFamily: 'Montserrat', background:"rgb(30, 131, 172)", height:"52px", display: 'flex', alignItem:"center", borderRadius: "4px", justifyContent: 'center',}} type="submit" color="primary" className="" block>
                                       

                                       {(this.state.isLoading)?
                                          (<BounceLoader  					
                                                className="clip-loader center"
                                                sizeUnit={"px"}
                                                size={25}
                                                color={'#fff'}
                                                loading={true} 
                                          />):'Enrégistrer'

                                       }
                                    </Button>
                                 </div>
                              </Col>
                           </FormGroup>
                        </Form>
                     </CardBody>
                  </Card>
               </Col>
            </Row>
         </div>
      );
   }
}

export default ResetPassword;
