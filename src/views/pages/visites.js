// import external modules
import React, { Fragment, Component } from "react";
import { Row, Card, Form, FormGroup, Label, Input, CardBody, Col, Button, CardHeader, Table, ModalHeader, ModalBody, ModalFooter, Modal } from "reactstrap";
import { User, CloudOff, Plus } from "react-feather";


class Visites extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false
        }

    }

  
     toggle = () => {
        this.setState({
           modal: !this.state.modal
        });
     }


   render() {
      return (
        <Fragment>
          <Row className="">
            <Col className="col-sm-12 col-xs-12 col-md-8 col-lg-8">
              <Card className="">
                <CardHeader className="bg-light">
                  <Form>
                    <Row>
                      <Col className="col-sm-6 col-xs-12 col-md-4 col-lg-4">
                        <FormGroup className="mb-0">
                          <Label for="timesheetinput1">Recherche par date</Label>
                            <div className="position-relative mb-0">
                                <Input type="date" />
                            </div>
                        </FormGroup>
                      </Col>
                      <Col className="col-sm-6 col-xs-12 col-md-4 col-lg-4 d-flex justify-content-start">
                        <FormGroup className="align-self-end mb-0">
                          <div className="position-relative mb-0 has-icon-left">
                            <Button style={{boxShadow:"0 2px 2px rgba(0,0,60,.08)",fontFamily: 'Montserrat', background:"rgb(30, 131, 172)", display: 'flex', alignItem:"center", borderRadius: "4px", justifyContent: 'center',}} className="mb-0 py-1 px-3 btn-raised">
                                AFFICHER
                            </Button>
                          </div>
                        </FormGroup>
                      </Col>
                      <Col className="col-sm-6 col-xs-12 col-md-4 col-lg-4 d-flex justify-content-end">
                        <FormGroup className="align-self-end mb-0">
                          <div className="position-relative mb-0">
                            <Button onClick={this.toggle} className="mb-0 p-1" style={{borderRadius: "18px",background:"rgb(30, 131, 172)"}}>
                                <Plus />
                            </Button>
                            <Modal
                            isOpen={this.state.modal}
                            toggle={this.toggle}
                            className={this.props.className}
                            >
                                <ModalHeader toggle={this.toggle}>Enregister une visite</ModalHeader>
                                <ModalBody>
                                    <Row>
                                        <Col className="col-md-6 col-lg-6 col-sm-6 col-12">
                                            <FormGroup>
                                                <Label for="projectinput2">Nom(s)</Label>
                                                <Input type="text" />
                                            </FormGroup>
                                        </Col>
                                        <Col className="col-md-6 col-lg-6 col-sm-6 col-12">
                                            <FormGroup>
                                                <Label for="projectinput1">Prénom(s)</Label>
                                                <Input type="text" />
                                            </FormGroup>
                                        </Col>
                                        <Col className="col-md-6 col-lg-6 col-sm-6 col-12">
                                            <FormGroup>
                                                <Label for="projectinput2">Téléphone</Label>
                                                <Input type="text" />
                                            </FormGroup>
                                        </Col>
                                        <Col className="col-md-6 col-lg-6 col-sm-6 col-12">
                                            <FormGroup>
                                                <Label for="projectinput2">Email</Label>
                                                <Input type="email" />
                                            </FormGroup>
                                        </Col>
                                        <Col className="col-md-6 col-lg-6 col-sm-6 col-12">
                                            <FormGroup>
                                                <Label for="projectinput2">École</Label>
                                                <Input type="text" />
                                            </FormGroup>
                                        </Col>
                                        <Col className="col-md-6 col-lg-6 col-sm-6 col-12">
                                            <FormGroup>
                                                <Label for="projectinput2">Canal</Label>
                                                <Input type="select" id="projectinput5" name="interested" >
                                                    <option value="BO" >Bouche à oreille</option>
                                                    <option value="WEB">Web</option>
                                                    <option value="RADIO">Radio</option>
                                                    <option value="AFFICHE">Affiche/Flyers</option>
                                                    <option value="TV">Télévision</option>
                                                    <option value="AUTRE">Autres</option>
                                                </Input>
                                            </FormGroup>
                                        </Col>
                                        <Col className="col-md-12 col-lg-12 col-sm-12 col-12">
                                            <FormGroup>
                                                <Label for="projectinput2">Demande</Label>
                                                <input
                                                className="form-control"
                                                type="textarea"
                                                name="notes"
                                                id="notes"
                                                />
                                            </FormGroup>
                                        </Col>

                                    </Row>
                                    
                                </ModalBody>
                                <ModalFooter>
                                    <Button color="primary" onClick={this.toggle}>
                                        Valider
                                    </Button>
                                </ModalFooter>
                            </Modal>
                          </div>
                        </FormGroup>
                      </Col>
                    </Row>
                  </Form>
                </CardHeader>
                <CardBody className="">
                  <Row className="">
                    <Col className="col-sm-12 col-xs-12 col-md-12 col-lg-12">
                      <Table striped>
                        <thead>
                          <tr>
                            <th>Matricule </th>
                            <th>Nom et Prénom</th>
                            <th>Ecole</th>
                            <th>Email</th>
                            <th>Téléphone</th>
                            <th>Nationnalité</th>
                            <th>Canal</th>
                            <th>Demande</th>
                            <th>Date de la visite</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </Table>
                      <div style={{width: "100%"}} className="d-flex justify-content-center">
                        <div>
                          <CloudOff color="#cfcfcf" size={100} />
                          <br />
                          	
                          <p className="text-muted mt-1">Aucune donnée</p>
                        </div>
                      </div>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Fragment>
      );
   }

}
 
export default Visites;