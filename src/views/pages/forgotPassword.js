// import external modules
import React from "react";
import { NavLink } from "react-router-dom";
import { resetPassword } from '../../utility/APIutils';
import { Row, Col, Input, Form, FormGroup, Button, Card, CardBody, CardFooter } from "reactstrap";
import { Check, CheckCircle } from "react-feather";
import { BounceLoader } from "react-spinners";

class ForgotPassword extends React.Component {

   constructor(props){
      super(props);
      this.state = {
         usernameOremail: "",
         validity:{
            validateStatus: "",
            errorMsg: ""
         },
         apiReponse: "",
         isEmailSent: false,
         formEmpty: false,
         isLoading:false,
         handleError: {
            hasError: false,
            errorMsg:""
         }
      }

      this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit (event) {
      event.preventDefault();   

      const passwordRequest = {
         usernameOrEmail : this.state.usernameOremail
      } 

      this.setState({isLoading: true});

      resetPassword(passwordRequest)

      .then(response => {

         this.setState({
            usernameOremail: "",
            apiReponse: response.data.message,
            isEmailSent: true
         })           
         console.log("response ", response)
         this.setState({isLoading: false});
      }).catch(error => {
         this.setState({isLoading: false});
         this.setState({handleError:  {hasError: true, errorMsg: error.response.data.message}});
      });
   }

   render(){

      return (
         <div className="container">
            <Row className="full-height-vh">
            {(!this.state.isEmailSent)?
                  (<Col xs="12" className="d-flex align-items-center justify-content-center">
                  <Card className="gradient-indigo-purple text-center width-400">                  
                     <CardBody>
                        <div className="text-center">
                           <h4 className="text-uppercase text-bold-400 white py-4">Mot de passe oublié</h4>
                        </div>

                        {(this.state.handleError.hasError)?
                        <p className="white">
                           {this.state.handleError.errorMsg}
                        </p>:""
                        }
                        <Form onSubmit={this.handleSubmit} className="pt-2">
                           <FormGroup>
                              <Col md="12">
                                 <Input
                                    type="text"
                                    className="form-control py-3"
                                    name="usernameOremail"
                                    value={this.state.usernameOremail}
                                    onBlur={event =>{this.validateEmail(event.target.value)}}
                                    disabled={this.state.isLoading}
                                    onChange={event => this.setState({usernameOremail: event.target.value, handleError: { hasError: false,errorMsg:""}})}
                                    id="inputEmail"
                                    placeholder="Login ou adresse e-mail"
                                 />
                              </Col>
                           </FormGroup>
                           <FormGroup className="">
                              <Col md="12">
                                 <div className="text-center mt-3">
                                    <Button disabled={this.state.isLoading} style={{boxShadow:"0 2px 2px rgba(0,0,60,.08)",fontFamily: 'Montserrat', background:"rgb(30, 131, 172)", height:"52px", display: 'flex', alignItem:"center", borderRadius: "4px", justifyContent: 'center',}} className="text-center"  type="submit" color="primary" block>
                                       

                                       {(this.state.isLoading)?
                                                (<BounceLoader  					
                                                    className="clip-loader center"
                                                    sizeUnit={"px"}
                                                    size={25}
                                                    color={'#fff'}
                                                    loading={true} 
                                                />):'Valider'

                                            }
                                    </Button>
                                 </div>
                              </Col>
                           </FormGroup>
                        </Form>
                     </CardBody>
                     <CardFooter>
                        <div className="float-left white">
                           <NavLink exact className="text-white" to="/">
                              Connexion
                           </NavLink>
                        </div>
                        <div className="float-right white">
                        </div>
                     </CardFooter>
                  </Card>
               </Col>):""
               }

               {(this.state.isEmailSent)?
                  (<Col xs="12" className="d-flex align-items-center justify-content-center">

                     <Card className="text-center">                  
                        <CardBody>
                           <CheckCircle size={60} color="green" />
                           <p className="ml-3 mt-2 text-bold-500">{this.state.apiReponse}</p>
                        </CardBody>
                     </Card>
                  </Col>):""
               }

            </Row>
         </div>
      );

   }

   validateEmail = (email) => {
      if(!email) {

         this.setState({
            validity:{
               validateStatus: 'error',
               errorMsg: 'Merci de renseigner votre login ou adresse e-mail'     
            }
         })

      }else{

         this.setState({
            validity:{
               validateStatus: 'success',
               errorMsg: ""
            }
         })

      }
  }
};

export default ForgotPassword;
