export const API_BASE_URL = process.env.REACT_APP_API_BASE_URL || 'http://localhost:5000';
export const API_BASE_URL_AUTH = process.env.REACT_APP_API_BASE_URL || 'http://localhost:4000';
export const API_ACCOUNT_BASE_URL = process.env.REACT_APP_API_BASE_URL || 'http://localhost:4000/api/user';
export const ACCESS_TOKEN = 'accessToken';

export const NAME_MIN_LENGTH = 4;
export const NAME_MAX_LENGTH = 40;

export const USERNAME_MIN_LENGTH = 3;
export const USERNAME_MAX_LENGTH = 15;

export const EMAIL_MAX_LENGTH = 40;

export const PASSWORD_MIN_LENGTH = 6;
export const PASSWORD_MAX_LENGTH = 20;