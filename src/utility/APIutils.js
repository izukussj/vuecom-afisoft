import { API_BASE_URL, ACCESS_TOKEN, API_ACCOUNT_BASE_URL } from '../constants';
import axios from 'axios';

const requestLogin = (options) => {

    return axios(options.url, options); 

};

const request = (options) => {

    return axios(options.url, options);

};

export function login(loginRequest) {

    let formBody = [];

    for (let property in loginRequest) {
        let encodedKey = encodeURIComponent(property);
        let encodedValue = encodeURIComponent(loginRequest[property]);
        formBody.push(encodedKey + "=" + encodedValue);
    }
    let resultformBody = formBody.join("&");

    return requestLogin({
        url: API_BASE_URL + "/oauth/token",
        method: 'POST',
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": 'Basic ' + window.btoa(loginRequest.username + ":" + "afiClientPassword")
          },
        data: resultformBody
    });
}

export function resetPassword(passwordRequest) {
    return request({
        url: API_BASE_URL + "/api/auth/forgot-password",
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        data: JSON.stringify(passwordRequest)
    });
}

export function changerPassword(changerPasswordRequest) {
    
    return request({
        url: API_BASE_URL + "/api/auth/change-password",
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        data: JSON.stringify(changerPasswordRequest)
    });
}

export function register(signupRequest) {
    return request({
        url: API_ACCOUNT_BASE_URL + "/signup",
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer "+ localStorage.getItem(ACCESS_TOKEN)
        },
        data: JSON.stringify(signupRequest)
    });
}

export function me(token) {
    return request({
        url: "http://localhost:4000/api/user/me",
        headers: {
            'Content-Type': 'application/json',
            "Authorization": 'bearer '+token
        },
        method: 'GET'
    });
}


export function checkLoginAvailability(login) {
    return request({
        url: API_ACCOUNT_BASE_URL + "/checkLoginAvailability/"+login,
        method: 'GET',
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer "+ localStorage.getItem(ACCESS_TOKEN)
        },
    });

}
export function getCurrentUser() {

    if(!localStorage.getItem(ACCESS_TOKEN)) {
        return Promise.reject("No access token set.");
    }

    let token = localStorage.getItem(ACCESS_TOKEN);

    return request({
        url: API_ACCOUNT_BASE_URL + "/me",
        headers: {
            'Content-Type': 'application/json',
            "Authorization": 'bearer '+token
        },
        method: 'GET'
    });
}

function refreshAccessToken() {

    if(!localStorage.getItem("refresh_token") || !localStorage.getItem("refresh_token")){
        return;
    }

    let username = localStorage.getItem("username");

    const payload = {
        grant_type: "refresh_token",
        refresh_token: localStorage.getItem("refresh_token"),
     } 
    
     let formBody = [];

    for (let property in payload) {
        let encodedKey = encodeURIComponent(property);
        let encodedValue = encodeURIComponent(payload[property]);
        formBody.push(encodedKey + "=" + encodedValue);
    }
    let resultformBody = formBody.join("&");

    return requestLogin({
        url: API_BASE_URL + "/oauth/token",
        method: 'POST',
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": 'Basic ' + window.btoa(username + ":" + "afiClientPassword")
          },
        data: resultformBody
    });
    
}

axios.interceptors.response.use(function (response) {

    return response;
  }, function (error) {
    const { config, response: { status } } = error;

    if (status === 401 && config.url !== "http://localhost:5000/oauth/token") {
            
        console.log("refreshing...")
        refreshAccessToken()
        .then(response => {
            console.log("response : ", response)
            localStorage.setItem(ACCESS_TOKEN, response.data.access_token);
            localStorage.setItem("refresh_token", response.data.refresh_token);
            localStorage.setItem("expires_in", response.data.expires_in);
        });
        
    }else{
        return Promise.reject(error);
    }
    
});