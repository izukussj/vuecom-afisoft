const viewReducer = (state = {view: "ETUDE"}, action) => {  
    switch (action.type) {
      case 'SET_VIEW':
        return {view: action.view}
      default:
        return state
    }
  }
  
export default viewReducer;