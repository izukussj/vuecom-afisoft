// import external modules
import { combineReducers } from "redux";

import loginReducer from "./login/loginReducer"
import userReducer from "./user/userReducer";
import viewReducer from "./view/viewReducer";

import { reducer as toastrReducer } from "react-redux-toastr";

import customizer from "./customizer/";

const rootReducer = combineReducers({
   toastr: toastrReducer, // <- Mounted at toastr.
   login: loginReducer,
   customizer: customizer,
   user: userReducer,
   view: viewReducer
});

export default rootReducer;
