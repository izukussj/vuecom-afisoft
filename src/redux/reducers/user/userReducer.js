const userReducer = (state = {user: []}, action) => {  
    switch (action.type) {
      case 'SET_USER':
        return {
          user: action.user
        }
      case 'SET_LOGOUT':
          return {isAuthenticated: false}
      default:
        return state
    }
  }
  
export default userReducer