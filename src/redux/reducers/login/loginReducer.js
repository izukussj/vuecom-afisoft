
const loginReducer = (state = {isAuthenticated: false}, action) => {  
    switch (action.type) {
      case 'SET_LOGIN':
        return {
          isAuthenticated: true
        }
      case 'SET_LOGOUT':
          return {isAuthenticated: false}
      default:
        return state
    }
  }
  
export default loginReducer