export const setView = (view) => {
    return {
       type: "SET_VIEW",
       view
     }
 }
