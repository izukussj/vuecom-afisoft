// import external modules
import React, { Component, Suspense, lazy } from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import Spinner from "../components/spinner/spinner";

// import internal(own) modules
import FullPageLayout from "../layouts/routes/fullpageRoutes";
import MainLayoutRoutes from "../layouts/routes/mainRoutes";


// Full Layout
const LazyForgotPassword = lazy(() => import("../views/pages/forgotPassword"));
const LazyLogin = lazy(() => import("../views/pages/login"));

const LazyResetPassword = lazy(() => import("../views/pages/resetPassword"));
const LazyNewAccount = lazy(() => import("../views/pages/NewAccount"));
const LazyAlumni = lazy(() => import("../views/pages/alumni"));
const LazyListEtudiant = lazy(() => import("../views/pages/listEtudiant"));
const LazyTuteurs = lazy(() => import("../views/pages/tuteurs"));
const LazyGesProg = lazy(() => import("../views/pages/gesProgramme"));
const LazyLogout = lazy(() => import("../views/pages/logout"));
const LazyGesClasse = lazy(() => import("../views/pages/gesClasse"));
const LazyAcaYear = lazy(() => import("../views/pages/AcaYear"));
const LazyAcaConfig = lazy(() => import("../views/pages/acaConfig"));
const LazyGesMention = lazy(() => import("../views/pages/gesMention"));
const LazyCarts = lazy(() => import("../views/pages/carts"));
const LazyFicheIns = lazy(() => import("../views/pages/ficheIns"));
const LazyVisites = lazy(() => import("../views/pages/visites"));
const LazyStateGlobal = lazy(() => import("../views/pages/stateGlobal"));


class Router extends Component {
    
   render() {

      return (
         // Set the directory path if you are deplying in sub-folder
         <BrowserRouter basename="/">
            <Switch>
               {/* Dashboard Views */}
               {/* Saperate Pages Views */}
               <FullPageLayout
                  exact
                  path="/"
                  render={(matchprops) => (
                     <Suspense fallback={<Spinner />}>
                        <LazyLogin {...matchprops} />
                     </Suspense>
                  )}
               />
               <FullPageLayout
                  exact 
                  path="/changer-password/:token"
                  render={(matchprops) => (
                     <Suspense fallback={<Spinner />}>
                        <LazyResetPassword {...matchprops} />
                     </Suspense>
                  )}
               />
               <FullPageLayout
                  exact
                  path="/forgot-password"
                  render={(matchprops) => (
                     <Suspense fallback={<Spinner />}>
                        <LazyForgotPassword {...matchprops} />
                     </Suspense>
                  )}
               />
               <MainLayoutRoutes 
                  exact
                  path="/account/new/"
                  render={(matchprops) => (
                     <Suspense fallback={<Spinner />}>
                        <LazyNewAccount {...matchprops} />
                     </Suspense>
                  )}
               />

               <MainLayoutRoutes 
                  exact
                  path="/alumni/"
                  render={(matchprops) => (
                     <Suspense fallback={<Spinner />}>
                        <LazyAlumni {...matchprops} />
                     </Suspense>
                  )}
               />

               <MainLayoutRoutes 
                  exact
                  path="/etudiants/"
                  render={(matchprops) => (
                     <Suspense fallback={<Spinner />}>
                        <LazyListEtudiant {...matchprops} />
                     </Suspense>
                  )}
               />

               <MainLayoutRoutes 
                  exact
                  path="/tuteurs/"
                  render={(matchprops) => (
                     <Suspense fallback={<Spinner />}>
                        <LazyTuteurs {...matchprops} />
                     </Suspense>
                  )}
               />

               <MainLayoutRoutes 
                  exact
                  path="/programmes"
                  render={(matchprops) => (
                     <Suspense fallback={<Spinner />}>
                        <LazyGesProg {...matchprops} />
                     </Suspense>
                  )}
               />

               <MainLayoutRoutes 
                  exact
                  path="/logout"
                  render={(matchprops) => (
                     <Suspense fallback={<Spinner />}>
                        <LazyLogout {...matchprops} />
                     </Suspense>
                  )}
               />

               <MainLayoutRoutes 
                  exact
                  path="/classes"
                  render={(matchprops) => (
                     <Suspense fallback={<Spinner />}>
                        <LazyGesClasse {...matchprops} />
                     </Suspense>
                  )}
               />

               <MainLayoutRoutes 
                  exact
                  path="/anneeacad"
                  render={(matchprops) => (
                     <Suspense fallback={<Spinner />}>
                        <LazyAcaYear {...matchprops} />
                     </Suspense>
                  )}
               />

               <MainLayoutRoutes 
                  exact
                  path="/acaconfig"
                  render={(matchprops) => (
                     <Suspense fallback={<Spinner />}>
                        <LazyAcaConfig {...matchprops} />
                     </Suspense>
                  )}
               />

               <MainLayoutRoutes 
                  exact
                  path="/mentions"
                  render={(matchprops) => (
                     <Suspense fallback={<Spinner />}>
                        <LazyGesMention {...matchprops} />
                     </Suspense>
                  )}
               />

               <MainLayoutRoutes 
                  exact
                  path="/cartes"
                  render={(matchprops) => (
                     <Suspense fallback={<Spinner />}>
                        <LazyCarts {...matchprops} />
                     </Suspense>
                  )}
               />
               <MainLayoutRoutes 
                  exact
                  path="/pre-saisie"
                  render={(matchprops) => (
                     <Suspense fallback={<Spinner />}>
                        <LazyFicheIns {...matchprops} />
                     </Suspense>
                  )}
               />
               <MainLayoutRoutes 
                  exact
                  path="/visites"
                  render={(matchprops) => (
                     <Suspense fallback={<Spinner />}>
                        <LazyVisites {...matchprops} />
                     </Suspense>
                  )}
               />
               <MainLayoutRoutes 
                  exact
                  path="/states"
                  render={(matchprops) => (
                     <Suspense fallback={<Spinner />}>
                        <LazyStateGlobal {...matchprops} />
                     </Suspense>
                  )}
               />
            </Switch>
         </BrowserRouter>
      );
   }
}

export default Router;
